# Welcome to RobonikMQ

A library for effectively working with, and building hyper-elastic infrastructure with [RabbitMQ](https://www.rabbitmq.com/)
